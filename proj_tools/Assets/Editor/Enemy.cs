﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;


public class Enemy : ScriptableObject
{
    public string Name;
    public float Health;
    public Sprite texture;
    public Colliders collider;
    public Enemy(string name, float health, Colliders coll, Sprite text)
    {
        Name = name;
        Health = health;
        texture = text;
        collider = coll;
    }
}
