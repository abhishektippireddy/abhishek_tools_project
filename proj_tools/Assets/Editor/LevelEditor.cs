using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using UnityEditor.ShortcutManagement;
using System.Collections.Generic;
using System.IO;

//using Newtonsoft.Json;

public class LevelEditor : EditorWindow, IBindable
{
    //to save rhe level changes in the map
    Level myLevel;
    //to know which layer is selected
    bool layer1 = true, layer2 = false;
    //to store the extracted textures from the scriptable objects  
    List<ScriptableObject> textures=new List<ScriptableObject>();
    //To save the selected scriptable objects
    ScriptableObject scriptableObjJson;
    //to store the selected texture
    Sprite image;
    static LevelEditor _window;
    VisualElement LevelEditorRootElement;
    IMGUIContainer gridContainer;
    Vector2 scrollPos2;
    //initial object field for the level
    ObjectField levelObjectField;

    public IBinding binding { get; set; }
    public string bindingPath { get; set; }

    //creating a shortcut for the window
    [Shortcut("Level Editor", KeyCode.F9)]
    //creating a menu item for the level editor
    [MenuItem("Tools/Level Editor")]

    //creating a window with max width and height
    public static void ShowExample()
    {
        if (_window) _window.Close();
        _window = GetWindow<LevelEditor>();
        _window.minSize = new Vector2(1080, 1920);
        // _window.maxSize = new Vector2(1080, 1920);
        _window.titleContent = new GUIContent("Level Editor");
    }
    //numbers of rows and columns initially
    int rowCount = 30;
    int columnCount = 45;
    int cellsize = 32;
   //drawing the grid on function call
    void drawGrid()
    {
        try
        {

            for (int i = 0; i < myLevel.Layer1.Count; i++)
            {
                GUI.DrawTexture(new Rect(new Vector2(myLevel.layer1Cells[i].x * cellsize, myLevel.layer1Cells[i].y * cellsize), new Vector2(cellsize, cellsize)), myLevel.Layer1[myLevel.layer1Cells[i]]);
            }
            for (int i = 0; i < myLevel.Layer2.Count; i++)
            {
                GUI.DrawTexture(new Rect(new Vector2(myLevel.layer2Cells[i].x * cellsize, myLevel.layer2Cells[i].y * cellsize), new Vector2(cellsize, cellsize)), myLevel.Layer2[myLevel.layer2Cells[i]]);
            }

            for (int r = 0; r < rowCount + 1; r++)
            {
                EditorGUI.DrawRect(new Rect(new Vector2(0, r * cellsize), new Vector2(columnCount * cellsize, 1)), Color.white);
            }
            for (int c = 0; c < columnCount + 1; c++)
            {
                EditorGUI.DrawRect(new Rect(new Vector2(c * cellsize, 0), new Vector2(1, rowCount * cellsize)), Color.white);
            }
        }
        catch(System.Exception e)
        {
            Debug.Log(e);
        }
        
    }

    #region Mouse click callback

    //after the  mouse event from the grid
    void mouseUpGridCallBack(MouseUpEvent evt)
    {
        Vector2 drawCell = evt.localMousePosition;
        if (drawCell.x < columnCount * cellsize && drawCell.y < rowCount * cellsize)
        {
            Vector2Int cellNum = new Vector2Int();
            cellNum.x = (int)drawCell.x / cellsize;
            cellNum.y = (int)drawCell.y / cellsize;
            if (layer1&&image.texture!=null)
            {
                if (myLevel.layer1Cells.Contains(cellNum))
                {
                    myLevel.layer1Cells.Remove(cellNum);
                    myLevel.Layer1.Remove(cellNum);
                    myLevel.LayerOne.Remove(cellNum);
                    
                }
                else
                {
                    myLevel.layer1Cells.Add(cellNum);
                    myLevel.Layer1.Add(cellNum, image.texture);
                    myLevel.LayerOne.Add(cellNum, scriptableObjJson);
                }
                Repaint();
            }
            if (layer2 && image.texture != null)
            {
                if (myLevel.layer2Cells.Contains(cellNum))
                {
                    myLevel.layer2Cells.Remove(cellNum);
                    myLevel.Layer2.Remove(cellNum);
                    myLevel.LayerTwo.Remove(cellNum);
                }
                else
                {
                    myLevel.layer2Cells.Add(cellNum);
                    myLevel.Layer2.Add(cellNum, image.texture);
                    myLevel.LayerTwo.Add(cellNum, scriptableObjJson);
                }
                Repaint();
            }
        }
    }


    //mouse event on the sliders of row and column count
    void mouseUpRowSlider(int newvalue)
    {
        rowCount = newvalue;
    }

    void mouseUpColumnSlider(int newvalue)
    {
        columnCount = newvalue;
    }
    //mouse events from the layer1 and layer2 buttons
    void layer1Setup(MouseUpEvent evt)
    {
        layer1 = true;
        layer2 = false;
       
    }
    void layer2Setup(MouseUpEvent evt)
    {
        layer2 = true;
        layer1 = false;

    }

    //when the add button is clicked the function is called on the object to generate a json node and convert into json
    //and a Json file is generated 
    void addLevel(MouseUpEvent evt)
    {   
        Debug.Log(myLevel.LayerOne.Count);
        string json = JsonUtility.ToJson(levelObjectField.value);
        string gameObjects= JsonUtility.ToJson(myLevel);
        string level = json + gameObjects;
       
        using (FileStream fs = new FileStream("Assets/example.json", FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(myLevel.serialize().ToString());
            }
        }

    }
    #endregion
 
    //creating a visual tree for the elements
    private void createVisualTree(Object obj)
    {
        if (LevelEditorRootElement != null)
        {
            rootVisualElement.Remove(LevelEditorRootElement);
            rootVisualElement.Remove(gridContainer);
            LevelEditorRootElement = null;
            gridContainer = null;
        }

        if (obj != null)
        {
            //binding all the level elements
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/LevelEditor.uss");
            rootVisualElement.styleSheets.Add(styleSheet);
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/LevelEditor.uxml");
            LevelEditorRootElement = visualTree.CloneTree();
            rootVisualElement.Add(LevelEditorRootElement);
            var levelNameLabel = LevelEditorRootElement.Q<TextField>("levelNameLabel");
            var rows = LevelEditorRootElement.Q<SliderInt>("levelRows");
            var columns = LevelEditorRootElement.Q<SliderInt>("levelColumns");
            var addLevelButton = LevelEditorRootElement.Q<Button>("addLevel");
            //Binding the layer buttons
            var layer1Button = LevelEditorRootElement.Q<Button>("Layer1");
            var layer2Button = LevelEditorRootElement.Q<Button>("Layer2");

            if (levelObjectField != null)
            {
                myLevel = (Level)levelObjectField.value;
            }
            gridContainer = new IMGUIContainer(drawGrid)
            {
                style =
                {
                    maxHeight=rowCount* cellsize,
                    maxWidth=columnCount* cellsize,

                    overflow = Overflow.Visible,
                  flexGrow = 1.0f,
                  flexShrink = 0.0f,
                  flexBasis = 0.0f,
                   backgroundColor = new Color(0.4f, 0.4f, 0.4f, 1.0f)

                }
            };

            //getting the mouse events and adding elements to the root
            gridContainer.RegisterCallback<MouseUpEvent>(mouseUpGridCallBack);
            rootVisualElement.Add(gridContainer);
            SerializedObject serializedObject = new SerializedObject(obj);
            rootVisualElement.Bind(serializedObject);
            addLevelButton.RegisterCallback<MouseUpEvent>(addLevel);
            layer1Button.RegisterCallback<MouseUpEvent>(layer1Setup);
            layer2Button.RegisterCallback<MouseUpEvent>(layer2Setup);
            rows.RegisterCallback<ChangeEvent<int>>((aEv) => mouseUpRowSlider(aEv.newValue));
            columns.RegisterCallback<ChangeEvent<int>>((aEv) => mouseUpColumnSlider(aEv.newValue));

        }
    }
    //ongui containers for the sprites to be loaded from the scriptable objects
    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(new Vector2(1650, 150), new Vector2(300, 850)));
        {
            GUILayout.BeginVertical();
            {
                int i = -90;
                GUI.Box(new Rect(new Vector2(0, 0), new Vector2(240, 850)), "Sprites");
                scrollPos2 = GUILayout.BeginScrollView(scrollPos2);
                {
                    GUILayout.Label("", GUILayout.Width(250), GUILayout.Height(textures.Count * 300));
                    GUILayout.BeginVertical();
                    {
                        foreach (ScriptableObject scriptableObj in textures)
                        {
                            if(scriptableObj.GetType().Equals(typeof(Player)))
                            {
                                Player p = (Player)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = scriptableObj;
                                }
                                i += 120;
                            }
                            if (scriptableObj.GetType().Equals(typeof(Finish)))
                            {
                                Finish p = (Finish)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = scriptableObj;
                                }
                                i += 120;
                            }
                            if (scriptableObj.GetType().Equals(typeof(Enemy)))
                            {
                                Enemy p = (Enemy)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = p;
                                }
                                i += 120;
                            }
                            if (scriptableObj.GetType().Equals(typeof(Spawn)))
                            {
                                Spawn p = (Spawn)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = p;
                                }
                                i += 120;
                            }
                            if (scriptableObj.GetType().Equals(typeof(Item)))
                            {
                                Item p = (Item)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = p;
                                }
                                i += 120;
                            }
                            if (scriptableObj.GetType().Equals(typeof(Start)))
                            {
                                Start p = (Start)scriptableObj;
                                GUIContent button = new GUIContent(p.name, p.texture.texture);
                                if (GUI.Button(new Rect(60, i + 150, 120, 120), button))
                                {
                                    image = p.texture;
                                    scriptableObjJson = p;
                                }
                                i += 120;
                            }
                        }
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }
    //called when the window is called
    public void OnEnable()
    {
        myLevel = ScriptableObject.CreateInstance<Level>();
        //array to store the scriptable objects
        string[] stringtoarray = AssetDatabase.FindAssets("t:ScriptableObject", new[] { "Assets/Resources/GameObjects" });
        textures.Clear();
        foreach (var guid1 in stringtoarray)
        {
            string test = AssetDatabase.GUIDToAssetPath(guid1);
            textures.Add((ScriptableObject)AssetDatabase.LoadMainAssetAtPath(test));
        }       
        //object field for level scriptable object
        levelObjectField = new ObjectField("Level");
        levelObjectField.objectType = typeof(Level);
        //value of the selected object
        levelObjectField.value = Selection.activeObject;
        levelObjectField.RegisterCallback<ChangeEvent<Object>>((evt) =>
        {
            createVisualTree(evt.newValue);
        });
        //adding it to the root element
        rootVisualElement.Add(levelObjectField);
        createVisualTree(Selection.activeObject as Level);
    }  
}