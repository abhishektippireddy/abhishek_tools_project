﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;

//creating a menu item
//[CreateAssetMenu(fileName = "Player-00", menuName = "Create/Player")]

//creating a scriptable object
public class Player : ScriptableObject
{
    public string Name;
    [Range(0, 100)]
    public float Health;
    public Sprite texture;
    public Colliders collider;
    public Player(string name, float health,Colliders coll,Sprite text)
    {
        Name = name;
        Health=health;
        texture = text;
        collider = coll;
    }
}
