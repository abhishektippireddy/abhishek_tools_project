﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;


public class Spawn : ScriptableObject
{
    public string Name;
    public float Health;
    public Sprite texture;
    public Colliders collider;
    float Time;
    Enemy enemy;
    public Spawn(string name, float health, Colliders coll, Sprite text,float time,Enemy emy)
    {
        Name = name;
        Health = health;
        texture = text;
        collider = coll;
        Time = time;
        enemy = emy;
    }
}
