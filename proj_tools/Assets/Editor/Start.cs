﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;


public class Start : ScriptableObject
{
    public string Name;
    public Sprite texture;
    public Colliders collider;
    public Start(string name,Colliders col, Sprite text)
    {
        Name = name;
        collider = col;
        texture = text;
    }
}
